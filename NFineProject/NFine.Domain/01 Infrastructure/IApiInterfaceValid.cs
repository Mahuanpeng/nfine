﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFine.Domain._01_Infrastructure
{
    /// <summary>
    /// 接口：API接口校验
    /// </summary>
    public interface IApiInterfaceValid
    {
        bool ValidInterfaceRequestCount(string apiName, string identityName, string requestIp, DateTime currentDateTime);
        bool ValidTokenRequestCount(string apiName, string identityName, string requestIp, DateTime currentDateTime);
    }
}
