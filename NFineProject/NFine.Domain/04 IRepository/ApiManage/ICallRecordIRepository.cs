﻿using NFine.Data;
using NFine.Domain._03_Entity.APIManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFine.Domain._04_IRepository.ApiManage
{
    public interface ICallRecordIRepository : IRepositoryBase<RequestRecordEntity>
    {
    }
}
