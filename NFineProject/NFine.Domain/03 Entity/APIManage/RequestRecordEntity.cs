﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFine.Domain._03_Entity.APIManage
{
    public class RequestRecordEntity
    {
        public string F_Id { get; set; }
        public string F_ApiName { get; set; }
        public int? F_Type { get; set; }
        public string F_TypeName { get; set; }
        public string F_RequestUser { get; set; }
        public string F_RequestIp { get; set; }
        public DateTime? F_RequestTime { get; set; }
        public int? F_Result { get; set; }
        public string F_Description { get; set; }
    }
}
