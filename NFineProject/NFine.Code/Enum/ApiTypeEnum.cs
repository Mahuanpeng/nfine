﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFine.Code.Enum
{
    /// <summary>
    /// 接口类型
    /// </summary>
    public enum ApiTypeEnum
    {
        [Description("令牌")]
        Token = 0,
        [Description("接口")]
        Interface = 1
    }
}
