﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFine.Code.Dictionary
{
    /// <summary>
    /// WEB.API字典
    /// </summary>
    public class ApiDictionary
    {
        public static Dictionary<int, string> ApiRequestDictionary = new Dictionary<int, string>
        {
            { 0, "调用失败" },
            { 1, "调用成功" },
            { 2, "每日调用次数达到上限" }
        };
    }
}
