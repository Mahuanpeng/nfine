﻿using NFine.Schedule.Quartz2._0.TimeLine;
using Quartz;
using Quartz.Impl;
using System.Collections.Specialized;

namespace NFine.Schedule.Quartz2._0
{
    /// <summary>
    /// Quartz调度管理类 master
    /// </summary>
    public class SchedulerManager
    {
        private static IScheduler _scheduler;
        private static IScheduler Scheduler
        {
            get
            {
                if (_scheduler != null)
                {
                    return _scheduler;
                }
                else
                {
                    //构造调度工厂
                    NameValueCollection props = new NameValueCollection
                    {
                        //调度名称
                        {"quartz.scheduler.instanceName", "卒中患者时间轴表更新作业调度" },
                        //设置线程池
                        //{ "quartz.threadPool.type", "Quartz.Simpl.SimpleThreadPool.Quartz"},   //2.3版本的Quartz版本不支持该属性
                        { "quartz.threadPool.threadCount", "3"},    //线程数3个，即最多同时执行3个作业
                        { "quartz.threadPool.threadPriority", "Normal"},   //线程池优先级，默认是5
                        //远程输出陪孩子
                        //{ "quartz.scheduler.exporter.type", "Quartz.Simpl.RemotingSchedulerExporter, Quartz" },
                        //{ "quartz.scheduler.exporter.port", "8008"},
                        //{ "quartz.scheduler.exporter.bindName", "QuartzScheduler"},
                        //{ "quartz.scheduler.exporter.channelType", "tcp"},
                        //数据存储路径
                        //{ "quartz.jobStore.type", "Quartz.Simpl.RAMJobStore, Quartz"},    //所有的Quartz的信息，包括作业和触发器都储存在内存中
                    };
                    StdSchedulerFactory factory = new StdSchedulerFactory(props);
                    _scheduler = factory.GetScheduler();
                    return _scheduler;
                }
            }
            set
            {
                value = _scheduler;
            }
        }
        /// <summary>
        /// 开始调度
        /// </summary>
        public static void ScheduleStart()
        {
            Scheduler.Start();

            TimeLineJobService timeLineJobService = new TimeLineJobService();
            Scheduler.ScheduleJob(timeLineJobService.GetJobDetail(), timeLineJobService.GetTrigger());
        }

        /// <summary>
        /// 调度结束
        /// </summary>
        public static void ScheduleStop()
        {
            _scheduler.Shutdown(true);
        }
    }
}
