﻿using NFine.Code;
using NFine.Code.Dictionary;
using NFine.Code.Enum;
using NFine.Domain._01_Infrastructure;
using NFine.Domain._03_Entity.APIManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFine.Application.ApiManage
{
    public class ApiInterfaceValidApp : IApiInterfaceValid
    {
        private readonly APIRequestRecordApp _requestRecordApp = new APIRequestRecordApp();

        /// <summary>
        /// 校验接口调用次数是否达到上限
        /// </summary>
        /// <param name="apiName"></param>
        /// <param name="identityName"></param>
        /// <param name="requestIp"></param>
        /// <param name="currentDateTime"></param>
        /// <returns></returns>
        public bool ValidInterfaceRequestCount(string apiName, string identityName, string requestIp, DateTime currentDateTime)
        {
            bool isValid;
            var requestRecordEntity = new RequestRecordEntity();
            requestRecordEntity.F_Id = Common.GuId();
            requestRecordEntity.F_ApiName = apiName;
            requestRecordEntity.F_Type = (int)ApiTypeEnum.Interface;
            requestRecordEntity.F_TypeName = "接口";
            requestRecordEntity.F_RequestUser = identityName;
            requestRecordEntity.F_RequestIp = requestIp;
            requestRecordEntity.F_RequestTime = DateTime.Now;

            var isRequestAlloded = _requestRecordApp.ValidAPIRequestNum(apiName, DateTime.Now, ApiTypeEnum.Interface);    //今日接口调用数是否达到上限
            if (isRequestAlloded)
            {
                //调用成功,记录调用者信息
                var key = 1;
                requestRecordEntity.F_Result = key;
                requestRecordEntity.F_Description = ApiDictionary.ApiRequestDictionary[key];
                _requestRecordApp.InsertEntity(requestRecordEntity);
                isValid = true; 
            }
            else
            {
                //调用失败,记录调用日志 
                var key = 2;
                requestRecordEntity.F_Result = key;
                requestRecordEntity.F_Description = ApiDictionary.ApiRequestDictionary[key];
                _requestRecordApp.InsertEntity(requestRecordEntity);
                isValid = false;
            }
            return isValid;
        }

        /// <summary>
        /// 查看Token调用是否达到上限
        /// </summary>
        /// <param name="identityName"></param>
        /// <param name="requestIp"></param>
        /// <param name="currentDateTime"></param>
        /// <returns></returns>
        public bool ValidTokenRequestCount(string apiName, string identityName, string requestIp, DateTime currentDateTime)
        {
            bool isValid;
            var requestRecordEntity = new RequestRecordEntity();
            requestRecordEntity.F_Id = Common.GuId();
            requestRecordEntity.F_ApiName = apiName;
            requestRecordEntity.F_Type = (int)ApiTypeEnum.Token;
            requestRecordEntity.F_TypeName = "Token";
            requestRecordEntity.F_RequestUser = identityName;
            requestRecordEntity.F_RequestIp = requestIp;
            requestRecordEntity.F_RequestTime = DateTime.Now;

            var isRequestAlloded = _requestRecordApp.ValidAPIRequestNum(apiName, DateTime.Now, ApiTypeEnum.Token);    //今日接口调用数是否达到上限
            if (isRequestAlloded)
            {
                //调用成功,记录调用者信息
                var key = 1;
                requestRecordEntity.F_Result = key;
                requestRecordEntity.F_Description = ApiDictionary.ApiRequestDictionary[key];
                _requestRecordApp.InsertEntity(requestRecordEntity);
                isValid = true;
            }
            else
            {
                //调用失败,记录调用日志 
                var key = 2;
                requestRecordEntity.F_Result = key;
                requestRecordEntity.F_Description = ApiDictionary.ApiRequestDictionary[key];
                _requestRecordApp.InsertEntity(requestRecordEntity);
                isValid = false;
            }
            return isValid;
        }
    }
}
