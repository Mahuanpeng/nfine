﻿using NFine.Code;
using NFine.Domain._03_Entity.APIManage;
using NFine.Domain._04_IRepository.ApiManage;
using NFine.Repository.ApiManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using NFine.Code.Enum;

namespace NFine.Application.ApiManage
{
    public class APIRequestRecordApp
    {
        private ICallRecordIRepository service = new CallRecordRepository();

        public IList<RequestRecordEntity> ListAllEntities()
        {
            return service.IQueryable().ToList();
        }

        public IList<RequestRecordEntity> ListEntitiesByExpression(Expression<Func<RequestRecordEntity, bool>> expression)
        {
            return service.IQueryable(expression).ToList();
        }

        public IList<RequestRecordEntity> ListEntitiesByPage(Pagination pagination, Expression<Func<RequestRecordEntity, bool>> expression)
        {
            return service.FindList(expression, pagination);
        }

        public void InsertEntity(RequestRecordEntity record)
        {
            service.Insert(record);
        }

        public void UpdateEntity(RequestRecordEntity record)
        {
            service.Update(record);
        }

        public void DeleteEntity(int id)
        {
            var record = service.FindEntity(id);
            if (record != null)
            {
                service.Delete(record);
            }
        }

        /// <summary>
        /// 校验API接口调用次数
        /// </summary>
        /// <param name="apiName">接口名称</param>
        /// <param name="dateTime">接口调用时间</param>
        public bool ValidAPIRequestNum(string apiName, DateTime dateTime, ApiTypeEnum type)
        {
            bool result = true;
            var requestBeginTime = DateTime.Parse(dateTime.ToString("yyyy-MM-dd"));
            var requestEndTime = DateTime.Parse(dateTime.ToString("yyyy-MM-dd")).AddDays(1);
            //判断条件包括 1：接口名 2：请求时间 3：是否请求成功
            IList<RequestRecordEntity> records = service.IQueryable(t => t.F_ApiName == apiName 
                                            && t.F_Type == (int)type
                                            && t.F_RequestTime >= requestBeginTime 
                                            && t.F_RequestTime < requestEndTime
                                            && t.F_Result == 1).ToList();
            var requestCountToday = records.Count();    //今日请求数
            int maxRequestCountOneDay = int.MaxValue;
            switch (type)
            {
                case ApiTypeEnum.Token:
                    maxRequestCountOneDay = Convert.ToInt32(ConfigurationSettings.AppSettings["MaxTokenRequestNum"]);
                    break;
                case ApiTypeEnum.Interface:
                    maxRequestCountOneDay = Convert.ToInt32(ConfigurationSettings.AppSettings["MaxApiRequestNum"]);
                    break;
                default:
                    break;
            }
            result = (requestCountToday > 0 && requestCountToday >= maxRequestCountOneDay) ? false : true;
            return result;
        }
    }
}
