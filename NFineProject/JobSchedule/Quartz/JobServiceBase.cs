﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobSchedule.Quartz
{
    /// <summary>
    /// 作业服务基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class JobServiceBase<T> where T : IJob
    {
        /// <summary>
        /// 作业名
        /// </summary>
        protected abstract string JobName { get; }
        /// <summary>
        /// 组名
        /// </summary>
        protected abstract string GroupName { get; }
        /// <summary>
        /// 创建作业对象
        /// </summary>
        /// <returns></returns>
        public IJobDetail GetJobDetail()
        {
            var job = JobBuilder.Create<T>()
                .WithIdentity(JobName, GroupName)
                .Build();
            return job;
        }
        /// <summary>
        /// 创建触发器
        /// </summary>
        /// <returns></returns>
        public abstract ITrigger GetTrigger();
    }
}
