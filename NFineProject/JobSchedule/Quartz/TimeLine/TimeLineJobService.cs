﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;

namespace NFine.Application.JobManage.TimeLine
{
    public class TimeLineJobService : JobServiceBase<TimeLineJob>
    {
        protected override string JobName => "时间轴节点时间更新作业";

        protected override string GroupName => "卒中患者定位系统";

        public override ITrigger GetTrigger()
        {
            //string corn = "0 0 9 ? * *";    //每天上午9:00执行
            string corn = "0/5 * * * * ?";    //测试: 每5秒执行一次
            ITrigger timeLineTrigger = TriggerBuilder.Create()
                     .WithIdentity("卒中患者定位系统", "作业触发器")
                     .WithCronSchedule(corn)
                     .Build();
            return timeLineTrigger;
        }
    }
}
