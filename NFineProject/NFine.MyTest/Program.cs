﻿using NFine.Schedule.Quartz2._0;
using System;

namespace NFine.MyTest
{
    /// <summary>
    /// 测试程序
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            SchedulerManager.ScheduleStart();
            Console.ReadLine();
            SchedulerManager.ScheduleStop();
        }
    }
}
