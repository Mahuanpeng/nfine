﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using NFine.Application.ApiManage;
using NFine.Code;
using NFine.Code.Dictionary;
using NFine.Code.Enum;
using NFine.Domain._03_Entity.APIManage;

namespace NFine.API.Filters
{
    /// <summary>
    /// API授权校验
    /// </summary>
    public class ApiAuthorizeFilterAttribute : AuthorizationFilterAttribute
    {
        private readonly APIRequestRecordApp _requestRecordApp = new APIRequestRecordApp();
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            HttpResponseMessage message = null;
            //1.是否OWIN授权
            var isOAuthValid = actionContext.RequestContext.Principal.Identity.IsAuthenticated;
            if (!isOAuthValid)
            {
                message = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError("Token is invalid"));
                actionContext.Response = message;
                return;
            }

            //2. 接口调用次数是否达到上限
            var requestAPIName = actionContext.Request.RequestUri.AbsolutePath;
            var identityName = actionContext.RequestContext.Principal.Identity.Name;
            var requestIp = actionContext.Request.RequestUri.Host;
            ApiInterfaceValidApp apiInterfaceValidApp = new ApiInterfaceValidApp();
            if (!apiInterfaceValidApp.ValidInterfaceRequestCount(requestAPIName, identityName, requestIp, DateTime.Now))
            {
                message = actionContext.Request.CreateErrorResponse(HttpStatusCode.Forbidden, new HttpError("API request up to top limit today."));
                actionContext.Response = message;
            }
            //待添加其他认证规则
        }
    }
}