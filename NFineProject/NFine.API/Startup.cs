﻿using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using NFine.API.OwinProviders;
using Owin;
using System;
using System.Web.Http;

namespace NFine.API
{
    /// <summary>
    /// OWIN 2.0启动入口
    /// </summary>
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigOAuth(app);

            //web.api注册token生成路由
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
            app.UseCors(CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        //开启OAuth服务
        public void ConfigOAuth(IAppBuilder app)
        {
            var expiresUtcMiniutes = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["TokenExpireTime"]);    //过期时间
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/oauth2/token"),     //获取 access_token 授权服务请求地址，即http://localhost:端口号/oauth2/token；
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(expiresUtcMiniutes),    //access_token 过期时间
                Provider = OwinProviderFactory.CreateProvider(),    //OAuth认证策略
                RefreshTokenProvider = OwinProviderFactory.CreateRefreshTokenProvider()     //refresh_token授权服务
            };
            //生成token
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}