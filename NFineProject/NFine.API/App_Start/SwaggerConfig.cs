using System.Web.Http;
using WebActivatorEx;
using NFine.API;
using Swashbuckle.Application;
using System.Xml.XPath;
using System;
using NFine.API.Swagger;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace NFine.API
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "NFine.API");
                        c.IncludeXmlComments(GetXmlCommentsPath());
                        c.CustomProvider((defaultProvider) => new SwaggerCacheProvider(defaultProvider, string.Format(@"{0}\bin\WebApiSwagger.XML", System.AppDomain.CurrentDomain.BaseDirectory)));
                        c.OperationFilter<HttpAuthHeaderFilter>();
                        c.DocumentFilter<AuthTokenOperationFilter>();
                    })
                .EnableSwaggerUi(c =>
                    {
                        c.DocumentTitle("系统接口_Swagger文档");
                        c.InjectJavaScript(System.Reflection.Assembly.GetExecutingAssembly(), "NFine.API.Swagger.swaggerui.swagger.js");    //添加中文注释
                    });
        }
        /// <summary>
        /// 获取XML注释文件所在路径
        /// </summary>
        /// <returns></returns>
        protected static string GetXmlCommentsPath()
        {
            return String.Format(@"{0}\bin\WebApiSwagger.XML", AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}
