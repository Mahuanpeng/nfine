﻿using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;

namespace NFine.API.Swagger
{
    /// <summary>
    /// Swagger手动添加获取Token令牌端点
    /// </summary>
    public class AuthTokenOperationFilter : IDocumentFilter
    {
        /// <summary>
        /// Apply方法重写
        /// </summary>
        /// <param name="swaggerDoc"></param>
        /// <param name="schemaRegistry"></param>
        /// <param name="apiExplorer"></param>
        public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
        {
            swaggerDoc.paths.Add("/oauth2/token",
                new PathItem
                {
                    post = new Operation {
                        tags = new List<string> { "OAuth2.Token" },
                        summary = "请求OAuth令牌",
                        consumes = new List<string> {
                            "application/x-www-form-urlencoded"
                        },
                        parameters = new List<Parameter> {
                            new Parameter{
                                type = "string",
                                name = "grant_type",
                                required = true,
                                @in = "formData",
                                description = "默认\"password\""
                            },
                            new Parameter{
                                type = "string",
                                name = "username",
                                required = true,
                                @in = "formData",
                                description = "账号"
                            },
                            new Parameter{
                                type = "string",
                                name = "password",
                                required = true,
                                @in = "formData",
                                description = "密码"
                            }
                        }
                    }
                });
        }
    }
}