﻿using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace NFine.API.OwinProviders
{
    public class SimpleRefreshTokenPrivider : IAuthenticationTokenProvider
    {
        private readonly ConcurrentDictionary<string, string> _refreshTokens = new ConcurrentDictionary<string, string>(StringComparer.Ordinal);

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });   //允许所有域名的脚本访问该资源
            string value;
            if (_refreshTokens.TryRemove(context.Token, out value))
            {
                context.DeserializeTicket(value);
            }
            return Task.FromResult<object>(null);
        }

        public Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            context.Ticket.Properties.IssuedUtc = DateTime.UtcNow;
            context.Ticket.Properties.ExpiresUtc = DateTime.UtcNow.AddMinutes(Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["TokenExpireTime"]));
            context.SetToken(Guid.NewGuid().ToString("n"));

            _refreshTokens[context.Token] = context.SerializeTicket();

            return Task.FromResult<object>(null);
        }
    }
}