﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFine.API.OwinProviders
{
    /// <summary>
    /// 中间件服务注册工厂类
    /// </summary>
    public class OwinProviderFactory
    {
        private static T GetInstance<T>()
            where T : new()
        {
            return Activator.CreateInstance<T>();
        }
        public static SimpleAuthorizationProvider CreateProvider()
        {
            return GetInstance<SimpleAuthorizationProvider>();
        }
        public static SimpleRefreshTokenPrivider CreateRefreshTokenProvider()
        {
            return GetInstance<SimpleRefreshTokenPrivider>();
        }
    }
}