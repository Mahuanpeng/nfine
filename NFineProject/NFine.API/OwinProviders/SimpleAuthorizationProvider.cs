﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using NFine.Application.ApiManage;
using NFine.Application.SystemManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace NFine.API.OwinProviders
{
    public class SimpleAuthorizationProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //允许所有域名的脚本访问该资源
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            //1. Token调用次数是否达到上限
            var requestAPIName = "/oauth2/token";
            var identityName = context.UserName;
            var requestIp = context.Request.RemoteIpAddress;
            ApiInterfaceValidApp apiInterfaceValidApp = new ApiInterfaceValidApp();
            if (!apiInterfaceValidApp.ValidTokenRequestCount(requestAPIName, identityName, requestIp, DateTime.Now))
            {
                context.SetError("API Request up to top limit");
                return;
            }

            //2.用户认证逻辑
            UserApp userApp = new UserApp();
            if (!userApp.CheckWebAPIOAuthUser(context.UserName, context.Password))
            {
                context.SetError("invalid_grant", "The username or password is incorrect.");
                return;
            }
            //采用了Claims Identity认证方式
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
            var ticket = new AuthenticationTicket(identity, new AuthenticationProperties() { AllowRefresh = true });
            context.Validated(ticket);
            await base.GrantResourceOwnerCredentials(context);
        }
    }
}