﻿using ICSharpCode.SharpZipLib.Zip;
using NFine.API.Filters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace NFine.API.Controllers.UploadManage
{
    /// <summary>
    /// 文件上传接口
    /// </summary>
    public class FileUploadController : ApiController
    {
        #region 文件上传
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> UploadFile()
        {
            if (Request.Content.IsMimeMultipartContent("form-data"))
            {
                try
                {
                    var form = HttpContext.Current.Request.Form;
                    var destFilePaths = new List<string>();    //上传文件存储路径
                    var filePath = ConfigurationSettings.AppSettings["UplaodFilePath"];
                    var fileFolderPath = HttpContext.Current.Server.MapPath(filePath);

                    if (!Directory.Exists(fileFolderPath))
                    {
                        Directory.CreateDirectory(fileFolderPath);
                    }
                    var streamProvider = new MultipartFormDataStreamProvider(fileFolderPath);
                    await Request.Content.ReadAsMultipartAsync(streamProvider);
                    
                    foreach (MultipartFileData fileData in streamProvider.FileData)
                    {
                        var sourceFileName = fileData.Headers.ContentDisposition.FileName.TrimStart('"').TrimEnd('"');
                        var fileExtension = Path.GetExtension(sourceFileName);

                        FileInfo fileInfo = new FileInfo(fileData.LocalFileName);
                        if (fileInfo.Length > 0 && fileInfo.Length < FileMaxSize)
                        {
                            if (!String.IsNullOrEmpty(fileExtension) && Array.IndexOf(FileExtensions.Split(','), fileExtension.Substring(1).ToLower()) > 0)
                            {
                                var destFileName = Guid.NewGuid().ToString().Replace("-", String.Empty) + fileExtension;
                                var destFilePath = Path.Combine(fileFolderPath, destFileName);
                                File.Move(fileData.LocalFileName, destFilePath);

                                destFilePaths.Add(destFilePath.Replace(@"\", "/"));
                            }
                            else
                            {
                                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "上传文件格式不支持"));
                            }
                        }
                        else
                        {
                            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "上传文件大小超过上限"));
                        }
                    }

                    return Ok(new { message = "上传成功", filePaths = destFilePaths });
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return BadRequest("This request is not properly formatted");
            }
        }
        #endregion

        #region 文件下载
        /// <summary>
        /// 普通下载文件
        /// </summary>
        /// <param name="fileName">文件名（带拓展名）</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage DownloadFile(string fileName)
        {
            HttpResponseMessage responseMessage = null;
            var fileFolderPath = ConfigurationSettings.AppSettings["UplaodFilePath"];
            DirectoryInfo directoryInfo = new DirectoryInfo(HttpContext.Current.Server.MapPath(fileFolderPath));     //通过绝对路径查找指定的文件
            FileInfo fileInfo = directoryInfo.GetFiles().Where(f => f.Name == fileName).FirstOrDefault();
            if (fileInfo != null)
            {
                FileStream fileStream = new FileStream(fileInfo.FullName, FileMode.Open);
                responseMessage = new HttpResponseMessage(HttpStatusCode.OK);
                responseMessage.Content = new StreamContent(fileStream);
                responseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                responseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                responseMessage.Content.Headers.ContentDisposition.FileName = fileInfo.Name;
            }
            else
            {
                responseMessage = new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            return responseMessage;
        }

        /// <summary>
        /// 下载压缩文件
        /// </summary>
        /// <param name="fileNameStr">文件名字符串集合，用','分隔</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage DownloadZipFil(string fileNames)
        {
            string compressFileName = DateTime.Now.ToString("yyyy-MM-dd") + ".zip";    //压缩后的文件名
            string fileFolderPath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["UplaodFilePath"]);
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                string[] fileNameArray = fileNames.Split(',');
                using (ZipOutputStream zipStream = new ZipOutputStream(File.Create(fileFolderPath + "/" + compressFileName)))
                {
                    zipStream.SetLevel(9);
                    byte[] buffer = new byte[4096];

                    foreach (var fileName in fileNameArray)
                    {
                        var entity = new ZipEntry(Path.GetFileName(fileFolderPath + "/" + fileName));
                        entity.DateTime = DateTime.Now;
                        zipStream.PutNextEntry(entity);
                        using (FileStream fileStream = File.OpenRead(fileFolderPath + "/" + fileName))
                        {
                            int index;
                            do
                            {
                                index = fileStream.Read(buffer, 0, buffer.Length);
                                zipStream.Write(buffer, 0, index);
                            }
                            while (index > 0);
                        }
                    }
                    zipStream.Finish();
                    zipStream.Close();
                }
                FileStream fs = new FileStream(fileFolderPath + "/" + compressFileName, FileMode.Open,
                    FileAccess.Read, FileShare.Read);
                responseMessage.Content = new StreamContent(fs);
                responseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                responseMessage.Content.Headers.ContentDisposition.FileName = compressFileName;
                responseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");    //通知浏览器下载文件
                responseMessage.Content.Headers.ContentLength = new FileInfo(fileFolderPath + "/" + compressFileName).Length;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
            return responseMessage;
        }
        #endregion

        #region 图片上传
        /// <summary>
        /// 单图片上传
        /// </summary>
        /// <returns></returns>
        [HttpPost, AllowAnonymous]
        public async Task<IHttpActionResult> SingleImageUpload()
        {
            var form = HttpContext.Current.Request.Form;    //页面参数

            if (Request.Content.IsMimeMultipartContent("form-data"))    //用于判断HTTP请求的媒体类型是否是multipart/form-data 
            {
                //判断路由文件是否存在
                var virtualPath = ConfigurationSettings.AppSettings["UploadImagePath"];
                var imgSavePath = HttpContext.Current.Server.MapPath(virtualPath);
                if (!Directory.Exists(imgSavePath))
                {
                    Directory.CreateDirectory(imgSavePath);
                }
                var streamProvider = new MultipartFormDataStreamProvider(imgSavePath);    //流提供程序，提供正文部分的输出流
                await Request.Content.ReadAsMultipartAsync(streamProvider);

                var fileData = streamProvider.FileData[0];
                var sourceFileName = fileData.Headers.ContentDisposition.FileName.TrimStart('"').TrimEnd('"');
                var fileInfo = new FileInfo(fileData.LocalFileName);

                if (fileInfo.Length <= 0)
                {
                    return BadRequest("请选择上传文件");
                }
                else if (fileInfo.Length > ImageMaxSize)
                {
                    return BadRequest("上传文件大小超过上限");
                }
                else
                {
                    var fileExtension = Path.GetExtension(sourceFileName);
                    if (!String.IsNullOrEmpty(fileExtension) && Array.IndexOf(ImageExtensions.Split(','), fileExtension.Substring(1).ToLower()) > 0)
                    {
                        try
                        {
                            var destFileName = Guid.NewGuid().ToString().Replace("-", String.Empty) + fileExtension;
                            var destFilePath = Path.Combine(imgSavePath, destFileName);
                            File.Move(fileData.LocalFileName, destFilePath);

                            return Ok(new { message = "上传成功", filePath = destFilePath.Replace(@"\", "/") });
                        }
                        catch (System.Exception ex)
                        {
                            throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, ex));
                        }
                    }
                    else
                    {
                        return BadRequest("不支持上传类型");
                    }
                }
            }
            else
            {
                return BadRequest("This request is not properly formatted");
            }
        }

        /// <summary>
        /// 多图片上传
        /// </summary>
        /// <returns></returns>
        [HttpPost, AllowAnonymous]
        public async Task<IHttpActionResult> MultiImageUpload()
        {
            if (Request.Content.IsMimeMultipartContent("form-data"))    //用于判断HTTP请求的媒体类型是否是multipart/form-data 
            {
                //判断路由文件是否存在
                var virtualPath = ConfigurationSettings.AppSettings["UploadImagePath"];
                var imgSavePath = HttpContext.Current.Server.MapPath(virtualPath);
                if (!Directory.Exists(imgSavePath))
                {
                    Directory.CreateDirectory(imgSavePath);
                }
                var streamProvider = new MultipartFormDataStreamProvider(imgSavePath);    //流提供程序，提供正文部分的输出流
                await Request.Content.ReadAsMultipartAsync(streamProvider);

                var destFilePathArray = new List<string>();    //文件路径集合
                foreach (var fileData in streamProvider.FileData)
                {

                    var sourceFileName = fileData.Headers.ContentDisposition.FileName.TrimStart('"').TrimEnd('"');
                    var fileInfo = new FileInfo(fileData.LocalFileName);

                    if (fileInfo.Length <= 0)
                    {
                        return BadRequest("请选择上传文件");
                    }
                    else if (fileInfo.Length > ImageMaxSize)
                    {
                        return BadRequest("上传文件大小超过上限");
                    }
                    else
                    {
                        var fileExtension = Path.GetExtension(sourceFileName);
                        if (!String.IsNullOrEmpty(fileExtension) && Array.IndexOf(ImageExtensions.Split(','), fileExtension.Substring(1).ToLower()) > 0)
                        {
                            try
                            {
                                var destFileName = Guid.NewGuid().ToString().Replace("-", String.Empty) + fileExtension;
                                var destFilePath = Path.Combine(imgSavePath, destFileName);
                                File.Move(fileData.LocalFileName, destFilePath);

                                destFilePathArray.Add(destFilePath.Replace(@"\", "/"));
                            }
                            catch (System.Exception ex)
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, ex));
                            }
                        }
                        else
                        {
                            return BadRequest("不支持上传类型");
                        }
                    }
                }
                return Ok(new { message = "上传成功", filePath = destFilePathArray });
            }
            else
            {
                return BadRequest("This request is not properly formatted");
            }
        }


        #endregion

        #region 生成缩略图

        #endregion

        #region 限制参数
        /// <summary>
        /// 单图片大小限制（最大20Mb）
        /// </summary>
        private int ImageMaxSize = Convert.ToInt32(ConfigurationManager.AppSettings["ImageMaxSize"]);
        /// <summary>
        /// 单文件大小限制（最大1G）
        /// </summary>
        private int FileMaxSize = Convert.ToInt32(ConfigurationManager.AppSettings["FileMaxSize"]);
        /// <summary>
        /// 允许上传图片的拓展名
        /// </summary>
        private string ImageExtensions = ConfigurationManager.AppSettings["ImgExtensions"];
        /// <summary>
        /// 允许上传文件的拓展名
        /// </summary>
        private string FileExtensions = ConfigurationManager.AppSettings["FileExtensions"]; 
        #endregion
    }
}
