﻿using NFine.API.Filters;
using NFine.Application.SystemManage;
using NFine.Code;
using NFine.Domain.Entity.SystemManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NFine.API.Controllers.SystemManage
{
    /// <summary>
    /// 用户管理接口
    /// </summary>
    [RoutePrefix("api/UserManage")]
    public class UserManageController : ApiController
    {
        /// <summary>
        /// 测试
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok();
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        [HttpPost, ApiAuthorizeFilter]
        public IHttpActionResult UserLogin(string userName, string password)
        {
            UserApp userApp = new UserApp();
            UserEntity userEntity = userApp.CheckLogin(userName, Md5.md5(password, 32));
            return Json(userEntity);
        }
    }
}
