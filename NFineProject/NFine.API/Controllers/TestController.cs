﻿using NFine.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace NFine.API.Controllers
{
    /// <summary>
    /// 测试接口
    /// </summary>
    [ApiAuthorizeFilter]
    public class TestController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetAPI(int id)
        {
            if (id < 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(new { msg = "成功", total = 100 });
            }
        }

        /// <summary>
        /// 异常说明
        /// </summary>
        /// <returns></returns>
        [HttpPost, ActionName("MyAPI")]
        public IHttpActionResult GetAPI()
        {
            //1. 方法未实现异常
            //throw new NotImplementedException();

            //2. 可以在响应中写入内容
            //var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
            //{
            //    Content = new StringContent(string.Format("No product with ID = {0}", 1)),
            //    ReasonPhrase = "Product ID Not Found"
            //};
            //throw new HttpResponseException(resp);

            var entityObj = new
            {
                id = 1,
                name = ""
            };
            return Ok(entityObj);
        }

        [HttpDelete, ActionName("APIMessage"), AllowAnonymous]    //API无需任何授权
        public HttpResponseMessage APIMessage()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "value");
            response.Content = new StringContent("hello", Encoding.Unicode);
            response.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };
            return response;
        }
    }
}
