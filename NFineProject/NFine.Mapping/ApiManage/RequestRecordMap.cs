﻿using NFine.Domain._03_Entity.APIManage;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFine.Mapping.ApiManage
{
    public class RequestRecordMap : EntityTypeConfiguration<RequestRecordEntity>
    {
        public RequestRecordMap()
        {
            this.ToTable("Api_RequestRecord");
            this.HasKey(t => t.F_Id);
        }
    }
}
